package es.resource.searcher.service;

import es.resource.searcher.entities.User;

public interface AuthorizationService {

	public String generateToken(final User loginUser);
	
	public String regenerateToken();

	public String resetPassword(final User userReset);


	public boolean isValidatedToken(final String token);

	public User getUser(final String userId);
	
	public void sendEmail(final String recipientEmail, final String jwt);
}
