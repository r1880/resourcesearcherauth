package es.resource.searcher.entities;

import es.valhalla.data.access.mongodb.entities.BaseEntity;
import es.valhalla.jwt.entities.JWTRoleENUM;

public class User extends BaseEntity{
	
	
	private String name;
	private String surname;
	private boolean active;
	private String username;
	private String email;
	private String pwd;
	private byte[] salt;
	private JWTRoleENUM role;
	

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public JWTRoleENUM getRole() {
		return role;
	}

	public void setRole(JWTRoleENUM role) {
		this.role = role;
	}

	public byte[] getSalt() {
		return salt;
	}

	public void setSalt(byte[] salt) {
		this.salt = salt;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	
}
