package es.resource.searcher.constants;

public class AuthConstants {

	public static final String APP_NAME = "Auth";
	public static final String SERVICE_PATH = "/resourcesearcher/rest/service/auth";
	public static final int SERVICE_PORT = 7070;
	public static final String COLLECTION_NAME = "mongo.collection";
	public static final String EMAIL_SUBJECT = "email.subject";
	public static final String EMAIL_FRONT_URL = "email.front.url";	
	public static final String SERVICE_REGISTRY_PROP="service.registry.path";
		/**LOCAL PROPERTY**/
	public static final String DEFAULT_REGISTRY_PATH="C:\\ResoureSearcher\\resource_searcher.json";
}
