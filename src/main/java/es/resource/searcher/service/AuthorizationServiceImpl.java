package es.resource.searcher.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.resource.searcher.constants.AuthConstants;
import es.resource.searcher.entities.User;
import es.resource.searcher.route.AuthEndpoint;
import es.valhalla.business.component.AbstractBusinessComponent;
import es.valhalla.constants.ValahallaConstants;
import es.valhalla.jwt.entities.JWTRoleENUM;
import es.valhalla.utils.logger.ValhallaLogger;
import es.valhalla.utils.logger.impl.ValhallaLoggerImpl;
import jakarta.annotation.PostConstruct;

public class AuthorizationServiceImpl extends AbstractBusinessComponent implements AuthorizationService {

	private String url;
	private String databaseName;
	private String user;
	private String secret;
	private String collection;
	
	private final static ValhallaLogger LOGGER = ValhallaLoggerImpl.getInstance(AuthorizationServiceImpl.class);


	@PostConstruct
	private void init() {
		this.url = getStringProperty(AuthConstants.APP_NAME, ValahallaConstants.MONGO_URL);
		this.databaseName = getStringProperty(AuthConstants.APP_NAME, ValahallaConstants.MONGO_DBNAME);
		this.user = getStringProperty(AuthConstants.APP_NAME, ValahallaConstants.MONGO_USER);
		this.secret = getStringProperty(AuthConstants.APP_NAME, ValahallaConstants.MONGO_SECRET);
		this.collection = getStringProperty(AuthConstants.APP_NAME, AuthConstants.COLLECTION_NAME);
	}

	@Override
	public String generateToken(final User loginUser) {
		final User retrieved = validateUser(loginUser);
		if (retrieved != null) {
			return generateToken(retrieved.getRole());
		} else {
			return null;
		}
	}

	@Override
	public String regenerateToken() {
		return generateToken(JWTRoleENUM.RESET);
	}

	@Override
	public String resetPassword(final User userReset) {
		final User user = getUser(userReset.getId());
		if (user != null) {
			return generateToken(JWTRoleENUM.RESET);

		} else {
			return null;

			
		}
	}

	@Override
	public boolean isValidatedToken(final String token) {
		return isValidToken(token);
	}

	private User validateUser(final User userRh) {
		final Map<String,Object> map = new HashMap<>();
		if(userRh.getUsername()!=null) {
			map.put("_id", userRh.getUsername());
		}
		if(userRh.getEmail()!=null) {
			map.put("email", userRh.getEmail());

		}
		List<User> userList =find(url, user, secret, databaseName, collection, User.class,map); 
		User retrieved = userList!=null && !userList.isEmpty() ? userList.get(0) : null;
		if (retrieved != null) {
			LOGGER.info("USER: {} {}", retrieved.getUsername(), retrieved.getSalt());
			String hashed = hashPBKDF2ToString(userRh.getPwd(), retrieved.getSalt());
			retrieved = retrieved.getPwd().equals(hashed) ? retrieved : null;
		}
		return retrieved;

	}

	@Override
	public User getUser(final String userId) {
		return getMongo(url, user, secret, databaseName, collection, userId, User.class);
	}

	@Override
	public void sendEmail(final String recipientEmail, final String jwt) {
		final String subject = getStringProperty(AuthConstants.APP_NAME, AuthConstants.EMAIL_SUBJECT);
		sendEmail(AuthConstants.APP_NAME, recipientEmail, subject, emailContent(jwt, recipientEmail));
	}

	private String emailContent(final String jwt, final String user) {
		final String urlFront = getStringProperty(AuthConstants.APP_NAME, AuthConstants.EMAIL_FRONT_URL);
		final StringBuffer sb = new StringBuffer(urlFront);
		sb.append("resetPassword?token=");
		sb.append(jwt);
		sb.append("&user=");
		sb.append(user);
		return sb.toString();

	}
}
