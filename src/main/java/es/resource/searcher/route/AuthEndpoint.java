package es.resource.searcher.route;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.MediaType;

import es.resource.searcher.constants.AuthConstants;
import es.resource.searcher.entities.User;
import es.resource.searcher.service.AuthorizationService;
import es.valhalla.business.component.AbstractBaseHttpHandler;
import es.valhalla.entities.ValhallaServiceRegistry;
import es.valhalla.utils.logger.ValhallaLogger;
import es.valhalla.utils.logger.impl.ValhallaLoggerImpl;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.Router;
import jakarta.inject.Inject;

public class AuthEndpoint extends AbstractBaseHttpHandler {

	@Inject
	AuthorizationService authService;

	private Map<String, List<ValhallaServiceRegistry>> serviceRegistry;

	private final static ValhallaLogger LOGGER = ValhallaLoggerImpl.getInstance(AuthEndpoint.class);

	public void generateToken(final Router router) {
		final ValhallaServiceRegistry self = new ValhallaServiceRegistry(HttpMethod.POST.name(), AuthConstants.SERVICE_PORT,
				AuthConstants.SERVICE_PATH,getSelfAddress());
		serviceRegistry.get(AuthConstants.APP_NAME).add(self);
		buildRoute(router, AuthConstants.SERVICE_PATH, HttpMethod.POST, MediaType.APPLICATION_JSON,
				MediaType.APPLICATION_JSON).handler(ctx -> {
					LOGGER.info(INCOMING_REQUEST, ctx.request().uri(), ctx.request().method(),
							ctx.request().remoteAddress());
					final User loginUser = deserialize(ctx.getBodyAsString(), User.class);
					final String jwt = authService.generateToken(loginUser);
					if (jwt != null) {
						this.processSuccessHttpResponse(ctx, jwt);
					} else
						this.processUnauthorizedResponse(ctx);
				}).failureHandler(ctx -> unexpectedFailure(ctx, LOGGER));

	}

	public void resetPassword(final Router router) {
		final ValhallaServiceRegistry self = new ValhallaServiceRegistry(HttpMethod.PUT.name(), AuthConstants.SERVICE_PORT,
				AuthConstants.SERVICE_PATH,getSelfAddress());
		serviceRegistry.get(AuthConstants.APP_NAME).add(self);
		buildRoute(router, AuthConstants.SERVICE_PATH, HttpMethod.PUT, MediaType.APPLICATION_JSON,
				MediaType.APPLICATION_JSON).handler(ctx -> {
					LOGGER.info(INCOMING_REQUEST, ctx.request().absoluteURI(), ctx.request().method(),
							ctx.request().host());
					User user = authService.getUser(ctx.getBodyAsString());
					if (user != null) {
						final String jwtResest = authService.regenerateToken();
						authService.sendEmail(user.getUsername(), jwtResest);
						this.processSuccessHttpResponse(ctx);

					} else {
						this.processBadRequestResponse(ctx);

					}
				}).failureHandler(ctx -> unexpectedFailure(ctx, LOGGER));
	}

	public void validateToken(final Router router) {
		final ValhallaServiceRegistry self = new ValhallaServiceRegistry(HttpMethod.POST.name(), AuthConstants.SERVICE_PORT,
				AuthConstants.SERVICE_PATH + "/validate",getSelfAddress());
		serviceRegistry.get(AuthConstants.APP_NAME).add(self);
		buildRoute(router, AuthConstants.SERVICE_PATH + "/validate", HttpMethod.POST, MediaType.TEXT_PLAIN)
				.handler(ctx -> {
					LOGGER.info(INCOMING_REQUEST, ctx.request().absoluteURI(), ctx.request().method(),
							ctx.request().host());

					final String token = ctx.getBodyAsString();

					if (authService.isValidatedToken(token)) {
						this.processSuccessHttpResponse(ctx);
					} else
						this.processUnauthorizedResponse(ctx);
				}).failureHandler(ctx -> unexpectedFailure(ctx, LOGGER));
	}
	


	public void enableEndpoints(final Router router, final Map<String, List<ValhallaServiceRegistry>> serviceRegistry) {
		this.serviceRegistry = serviceRegistry;
		serviceRegistry.put(AuthConstants.APP_NAME, new ArrayList<>());
		generateToken(router);
		validateToken(router);
		resetPassword(router);
	}
}
